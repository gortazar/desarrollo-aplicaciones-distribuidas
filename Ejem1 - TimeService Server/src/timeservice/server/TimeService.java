package timeservice.server;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class TimeService  {
	
	@WebMethod
	public String getTimeAsString() {
		return new Date().toString();
	}

	@WebMethod
	public long getTimeAsElapsed() {
		return new Date().getTime();
	}
}