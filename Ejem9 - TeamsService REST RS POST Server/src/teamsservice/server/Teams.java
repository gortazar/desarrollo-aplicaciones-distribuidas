package teamsservice.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/")
public class Teams {

	private TeamsUtility utils;

	public Teams() {
		utils = new TeamsUtility();
	}

	@POST
	@Path("team")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Consumes(MediaType.TEXT_PLAIN)
	public Team getTeam(String name) {

		Team team = utils.getTeam(name);
		if (team == null) {
			throw new WebApplicationException(404); // not found
		}
		
		return team;
	}
	
	@POST
	@Path("teamjson")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public Team getTeamJSON(JSONObject json) throws JSONException {

		Team team = utils.getTeam(json.getString("name"));
		if (team == null) {
			throw new WebApplicationException(404); // not found
		}
		
		return team;
	}
	
	@POST
	@Path("teamjson2")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Consumes(MediaType.APPLICATION_JSON)
	public Team getTeamJSON2(TeamRequest request) throws JSONException {

		Team team = utils.getTeam(request.getName());
		if (team == null) {
			throw new WebApplicationException(404); // not found
		}
		
		return team;
	}

	@GET
	@Path("teams")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public TeamList getTeams() {
		return new TeamList(utils.getTeams());
	}

}