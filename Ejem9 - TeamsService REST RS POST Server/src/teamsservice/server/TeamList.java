package teamsservice.server;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TeamList {

	@XmlElement(name="team")
	private List<Team> list;
	
	public TeamList(){}
	
	public TeamList(List<Team> list) {
		this.list = list;
	}
	
	public List<Team> getList(){
		return list;
	}
	
}
