package es.sidelab.dad.rmi.randomgen;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RandomNumberGeneratorService extends Remote {

	List<Integer> generateIntegerRandomSequence(int sequenceLength, String clientId) throws RemoteException;
	List<Double> generateDoubleRandomSequence(int sequenceLength, String clientId) throws RemoteException;
	
}
