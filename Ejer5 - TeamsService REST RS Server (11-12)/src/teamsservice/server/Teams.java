package teamsservice.server;

import java.io.StringWriter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.thoughtworks.xstream.XStream;

@Path("/")
public class Teams {

	private TeamsData data = new TeamsData();
	
	@GET
	@Produces("text/xml")
	public String getResource(@QueryParam("name") String name) {
		if (name != null) {
			Team team = data.getTeam(name);
			return convertToXML(team);
		} else {
			return convertToXML(data.getTeams());
		}
	}

	private String convertToXML(Object object) {
		StringWriter writer = new StringWriter();
		XStream xstream = new XStream();
		xstream.alias("team", Team.class);
		xstream.alias("player", Player.class);
		xstream.autodetectAnnotations(true);
		xstream.toXML(object, writer);
		return writer.toString();
	}
}
