package teamsservice.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.sun.jersey.api.container.grizzly.GrizzlyWebContainerFactory;

public class Publisher {

	public static void main(String[] args) throws IllegalArgumentException, IOException {

		Map<String, String> config = new HashMap<String, String>();
		config.put("com.sun.jersey.config.property.packages",
				"teamsservice.server");
		
		GrizzlyWebContainerFactory
				.create("http://localhost:8888/teams", config);

	}

}
