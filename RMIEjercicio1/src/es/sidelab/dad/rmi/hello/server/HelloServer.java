package es.sidelab.dad.rmi.hello.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import es.sidelab.dad.rmi.hello.Hello;

public class HelloServer {

	public static void main(String[] args) {
		
		System.setSecurityManager(new RMISecurityManager());
		
		HelloImpl hello = new HelloImpl();
		try {
			
			Hello stub = (Hello) UnicastRemoteObject.exportObject(hello, 0);
			
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("hello", stub);
			
			System.out.println("Ready!");
			
		} catch (RemoteException e) {
			System.err.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			System.err.println("The name was already bound to an object");
			e.printStackTrace();
		}

	}

}
