package es.sidelab.dad.rmi.hello.server;

import es.sidelab.dad.rmi.hello.Hello;

public class HelloImpl implements Hello {

	@Override
	public String hello() {
		return "hello, RMI";
	}

}
