package es.sidelab.dad.rmi.hello.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import es.sidelab.dad.rmi.hello.Hello;

public class HelloClient {

	public static void main(String[] args) {
		
		try {
			
			Registry registry = LocateRegistry.getRegistry();
			Hello hello = (Hello) registry.lookup("hello");
			
			String salutation = hello.hello();
			System.out.println(salutation);
			
		} catch (RemoteException e) {
			System.err.println("Problem obtaining the registry: " + e.getMessage());
			e.printStackTrace();
		} catch (NotBoundException e) {
			System.err.println("Name not found: " + e.getMessage());
			e.printStackTrace();
		}
		

	}

}
