package es.sidelab.dad.rmi.hello;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Hello extends Remote {

	String hello() throws RemoteException;
	
}
