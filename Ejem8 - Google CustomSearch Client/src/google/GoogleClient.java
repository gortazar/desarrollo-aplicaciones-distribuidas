package google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GoogleClient {

	public static void main(String[] args) throws Exception {

		// More information about the REST service: 
		// http://code.google.com/apis/customsearch/v1/overview.html
		URL url = new URL("https://www.googleapis.com/customsearch/v1?key=AIzaSyCi0FXALSUCfX9RMLSgNaYfG1Coy_M_YFU&cx=017576662512468239146:omuauf_lfve&q=lectures");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Accept", "application/json");
		conn.connect();

		// Read InputStream as String
        String jsonResponse = buildString(conn.getInputStream());

        // Print the JSON response
		System.out.println(jsonResponse);		

	}

	private static String buildString(InputStream inputStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder sb = new StringBuilder();
		String line = reader.readLine();
		while(line != null){
			sb.append(line).append("\r\n");
			line = reader.readLine();
		}
		return sb.toString();
	}
	
}
