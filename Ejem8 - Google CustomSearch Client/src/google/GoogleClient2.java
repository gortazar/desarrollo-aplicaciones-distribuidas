package google;

import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class GoogleClient2 {

	public static void main(String[] args) throws Exception {

		// All Teams
		URL url = new URL(
				"https://www.googleapis.com/customsearch/v1?key=AIzaSyCi0FXALSUCfX9RMLSgNaYfG1Coy_M_YFU&cx=017576662512468239146:omuauf_lfve&q=lectures");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Accept", "application/json");
		conn.connect();

		//Read JSON info with Tree Model API of Jackson Library 
		ObjectMapper mapper = new ObjectMapper();	
		JsonNode rootNode = mapper.readValue(conn.getInputStream(), JsonNode.class);

		for (JsonNode node : rootNode.path("items")) {
			System.out.println("Title: "+node.get("title"));
			System.out.println("Link: "+node.get("link"));
			System.out.println("Snippet: "+node.get("snippet"));
			System.out.println();
		}

	}

}
