package es.sidelab.dad.rmi.calculator.impl;

import java.io.Serializable;
import java.rmi.RemoteException;

import es.sidelab.dad.rmi.calculator.Calculator;

public class CalculatorImpl implements Calculator, Serializable {

	private static final long serialVersionUID = -7360160466459922372L;

	@Override
	public double mean(double[] values) throws RemoteException {
		return 0;
	}

	@Override
	public double sum(double[] values) throws RemoteException {
		return 0;
	}

	@Override
	public double mult(double[] values) throws RemoteException {
		return 0;
	}

	@Override
	public double[] deviation(double[] values) throws RemoteException {
		return null;
	}

}
