package es.sidelab.dad.rmi.calculator.impl;

import es.sidelab.dad.rmi.calculator.Calculator;
import es.sidelab.dad.rmi.calculator.CalculatorService;

public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public Calculator getCalculator() {
		return new CalculatorImpl();
	}

}
