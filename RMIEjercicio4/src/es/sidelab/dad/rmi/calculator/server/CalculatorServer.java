package es.sidelab.dad.rmi.calculator.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import es.sidelab.dad.rmi.calculator.CalculatorService;
import es.sidelab.dad.rmi.calculator.impl.CalculatorServiceImpl;

public class CalculatorServer {

	public static void main(String[] args) {
		
		CalculatorServiceImpl calculatorImpl = new CalculatorServiceImpl();
		
		try {
			
			CalculatorService calculator = (CalculatorService) UnicastRemoteObject.exportObject(calculatorImpl, 0, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
			
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("calculator", calculator);
			
			System.out.println("Waiting for tasks...");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}

	}
}
