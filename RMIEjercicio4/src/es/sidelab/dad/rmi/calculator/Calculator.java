package es.sidelab.dad.rmi.calculator;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculator extends Remote {

	double mean(double[] values) throws RemoteException;
	double sum(double[] values) throws RemoteException;
	double mult(double[] values) throws RemoteException;
	double[] deviation(double[] values) throws RemoteException;

}
