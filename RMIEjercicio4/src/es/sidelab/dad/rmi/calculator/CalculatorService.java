package es.sidelab.dad.rmi.calculator;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalculatorService extends Remote {
	
	Calculator getCalculator() throws RemoteException;
	
}
