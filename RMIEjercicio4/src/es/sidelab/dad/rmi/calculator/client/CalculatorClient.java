package es.sidelab.dad.rmi.calculator.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

import es.sidelab.dad.rmi.calculator.Calculator;
import es.sidelab.dad.rmi.calculator.CalculatorService;

public class CalculatorClient {

	public static void main(String[] args) {

		try {
			
			Registry registry = LocateRegistry.getRegistry();
			CalculatorService cs = (CalculatorService) registry.lookup("calculator");
			
			Calculator calc = cs.getCalculator();
			
			double[] dev = calc.deviation(new double[]{1.0, 2.0, 3.0});
			System.out.println(Arrays.toString(dev));
			
			System.out.println(calc.sum(new double[]{1.0, 2.0, 3.0}));
			
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}


	}

}
