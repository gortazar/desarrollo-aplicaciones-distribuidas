package teamsservice.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

@Path("/")
public class Teams {

	private TeamsUtility utils;

	public Teams() {
		utils = new TeamsUtility();
	}

	@GET
	@Path("team")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Team getTeam(@QueryParam("name") String name) {

		Team team = utils.getTeam(name);
		if (team == null) {
			throw new WebApplicationException(404); // not found
		}
		
		return team;
	}

	@GET
	@Path("teams")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public TeamList getTeams() {
		return new TeamList(utils.getTeams());
	}

}