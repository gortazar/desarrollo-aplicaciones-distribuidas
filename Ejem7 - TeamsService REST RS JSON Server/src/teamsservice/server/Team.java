package teamsservice.server;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Team {
	
	private String name;
	private List<Player> players;
	
	public Team() {
	}

	public Team(String name, List<Player> players) {
		setName(name);
		setPlayers(players);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setRosterCount(int n) {
	} // no-op but needed for property

	public int getRosterCount() {
		return (players == null) ? 0 : players.size();
	}
}
