package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AnuncioController {

	@RequestMapping("/guardaranuncio")
	public String guardarAnuncio(Anuncio anuncio, Model model) {

		model.addAttribute("anuncio", anuncio);

		return "anuncio";
	}
}