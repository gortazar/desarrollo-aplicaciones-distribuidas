package teamsservice.server;

import java.io.StringWriter;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;

import com.thoughtworks.xstream.XStream;

@Path("/")
public class Teams {

	private TeamsUtility utils;

	public Teams() {
		utils = new TeamsUtility();
	}

	public Team getTeam(String name) {
		return utils.getTeam(name);
	}

	public List<Team> getTeams() {
		return utils.getTeams();
	}

	@GET
	@Produces("text/xml")
	public String getResource(@QueryParam("name") String name) {

		if (name != null) {
			Team team = getTeam(name);
			if (team == null) {
				throw new WebApplicationException(404); // not found
			}
			return convertToXML(team);
		} else {
			return convertToXML(getTeams());
		}
	}

	private String convertToXML(Object object) {
		
		// Serialize object to XML and return
		StringWriter writer = new StringWriter();

		XStream xstream = new XStream();
		xstream.alias("team", Team.class);
		xstream.alias("player", Player.class);
		xstream.autodetectAnnotations(true);
		xstream.toXML(object, writer);

		return writer.toString();
	}

}