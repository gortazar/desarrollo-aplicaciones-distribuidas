package teamsservice.server;

import java.util.List;

import javax.jws.WebService;
import javax.jws.WebMethod;


@WebService
public class Teams {

	private TeamsUtility utils = new TeamsUtility();

	@WebMethod
	public Team getTeam(String name) {
		return utils.getTeam(name);
	}

	@WebMethod
	public List<Team> getTeams() {
		return utils.getTeams();
	}
}