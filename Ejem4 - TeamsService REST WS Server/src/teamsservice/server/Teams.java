package teamsservice.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.BindingType;
import javax.xml.ws.Provider;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import com.thoughtworks.xstream.XStream;

//The class below is a WebServiceProvider rather than the more usual
//SOAP-based WebService. The service implements the generic Provider 
//interface rather than a customized SEI with designated @WebMethods.
@WebServiceProvider
// There are two ServiceModes: PAYLOAD, the default, signals that the service
// wants access only to the underlying message payload (e.g., the
// body of an HTTP POST request); MESSAGE signals that the service wants
// access to entire message (e.g., the HTTP headers and body).
@ServiceMode(value = javax.xml.ws.Service.Mode.MESSAGE)
// The HTTP_BINDING as opposed, for instance, to a SOAP binding.
@BindingType(value = HTTPBinding.HTTP_BINDING)
public class Teams implements Provider<Source> {

	@Resource
	protected WebServiceContext wsCtx;

	private TeamsUtility utils;

	public Teams() {
		utils = new TeamsUtility();
	}

	public Team getTeam(String name) {
		return utils.getTeam(name);
	}

	public List<Team> getTeams() {
		return utils.getTeams();
	}

	// This method handles incoming requests and generates the response.
	public Source invoke(Source request) {

		if (wsCtx == null)
			throw new RuntimeException("DI failed on wsCtx.");

		// Grab the message context and extract the request verb.
		MessageContext msgCtx = wsCtx.getMessageContext();

		String httpVerb = (String) msgCtx
				.get(MessageContext.HTTP_REQUEST_METHOD);

		httpVerb = httpVerb.trim().toUpperCase();

		// Act on the verb. To begin, only GET requests accepted.
		if (httpVerb.equals("GET")) {
			return doGet(msgCtx);
		} else {
			throw new HTTPException(405); // method not allowed
		}
	}

	private Source doGet(MessageContext msgCtx) {

		// Parse the query string.
		String queryString = (String) msgCtx.get(MessageContext.QUERY_STRING);

		// Get all teams.
		if (queryString == null) {
			byte[] teamsAsXML = convertToXML(getTeams());
			return new StreamSource(new ByteArrayInputStream(teamsAsXML));
		} else {

			// Get a named team.
			String name = getValueFromQuery("name", queryString);

			// Check if named team exists.
			Team team = getTeam(name);
			if (team == null) {
				throw new HTTPException(404); // not found
			}

			// Otherwise, generate XML and return.
			byte[] teamAsXML = convertToXML(team);
			return new StreamSource(new ByteArrayInputStream(teamAsXML));
		}

	}

	private String getValueFromQuery(String key, String query) {
		String[] parts = query.split("=");
		// Check if query string has form: name=<team name>
		if (!parts[0].equalsIgnoreCase(key)) {
			throw new HTTPException(400); // bad request
		}
		return parts[1].trim();
	}

	private byte[] convertToXML(Object object) {
		// Serialize object to XML and return
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		XStream xstream = new XStream();
		xstream.alias("team", Team.class);
		xstream.alias("player", Player.class);
		xstream.autodetectAnnotations(true);
		xstream.toXML(object, stream);

		return stream.toByteArray();
	}

}