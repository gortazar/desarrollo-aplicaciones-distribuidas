package es.sidelab.dad.rmi.bolsa;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Bolsa extends Remote {

	List<String> getEmpresas() throws RemoteException;
	double getValue(String empresa) throws RemoteException;
	
}
