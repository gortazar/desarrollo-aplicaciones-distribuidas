package es.sidelab.dad.rmi.bolsa.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import es.sidelab.dad.rmi.bolsa.Bolsa;

public class Client {

	public static void main(String[] args) {
		
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry();
			Bolsa bolsa = (Bolsa) registry.lookup("bolsa");

			System.out.println(bolsa.getEmpresas());
			
			System.out.println(bolsa.getValue("Inditex"));
			
			System.out.println(bolsa.getValue("Empresa invalida"));
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
	}
}
