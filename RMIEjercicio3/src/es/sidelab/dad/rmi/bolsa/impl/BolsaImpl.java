package es.sidelab.dad.rmi.bolsa.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.sidelab.dad.rmi.bolsa.Bolsa;

public class BolsaImpl implements Bolsa {

	Map<String, Double> bolsa = new HashMap<String, Double>();
	
	public BolsaImpl() {
	
		bolsa.put("Repsol YPF", 12.34);
		bolsa.put("Inditex", 34.56);
		
	}
	
	@Override
	public List<String> getEmpresas() throws RemoteException {

		List<String> empresas = new ArrayList<String>();
		for(String empresa : bolsa.keySet()) {
			empresas.add(empresa);
		}
		return empresas;
		
	}

	@Override
	public double getValue(String empresa) throws RemoteException {

		if(!bolsa.containsKey(empresa)) {
			throw new RemoteException("La empresa no existe");
		}
		
		return bolsa.get(empresa);
	}

}
