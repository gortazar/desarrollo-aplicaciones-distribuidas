package es.sidelab.dad.rmi.bolsa.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import es.sidelab.dad.rmi.bolsa.Bolsa;
import es.sidelab.dad.rmi.bolsa.impl.BolsaImpl;

public class Server {
	
	public static void main(String[] args) {
		
		BolsaImpl bolsa = new BolsaImpl();
		try {
			Bolsa stub = (Bolsa) UnicastRemoteObject.exportObject(bolsa, 0);
			
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("bolsa", stub);
			
			System.out.println("Waiting connections...");

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
		
		
	}

}
