package es.sidelab;

import org.springframework.data.repository.CrudRepository;

import es.sidelab.model.Player;

public interface PlayerRepository extends CrudRepository<Player, Long> {

}
