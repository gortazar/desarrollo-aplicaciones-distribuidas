package es.sidelab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplobbdd3Application {

    public static void main(String[] args) {
        SpringApplication.run(Ejemplobbdd3Application.class, args);
    }
}
