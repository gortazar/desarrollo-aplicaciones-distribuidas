package es.sidelab.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Team {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;

	// CascadeType.ALL fuerza a insertar los objetos player en la bbdd si no lo estaban ya
	// Y a recuperarlos cuando se cargue este objeto
	@OneToMany(cascade=CascadeType.ALL)
	private List<Player> players;

	public Team(){}

	public Team(String name, List<Player> players) {
		this.name = name;
		this.players = players;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public String getName() {
		return name;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
}
