package es.sidelab;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.sidelab.model.Team;

@RestController
public class TeamsRestController {

	@Autowired
	private TeamRepository teamRepository;

	@RequestMapping(value = "/teams", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addTeam(@RequestBody Team team) {

		
		teamRepository.save(team);
		return new ResponseEntity<Boolean>(true,HttpStatus.CREATED);
	}

	@RequestMapping(value = "/teams", method = RequestMethod.GET)
	public List<Team> getTeams() {
		return teamRepository.findAll();
	}

	@RequestMapping(value = "/team/{teamName}", method = RequestMethod.GET)
	public Team getTeam(@PathVariable("teamName") String teamName) {
		return teamRepository.findByName(teamName);
	}
}