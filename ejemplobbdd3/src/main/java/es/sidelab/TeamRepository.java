package es.sidelab;

import org.springframework.data.jpa.repository.JpaRepository;

import es.sidelab.model.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
	
	Team findByName(String name);

}
