package es.sidelab.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import es.sidelab.PracticaRest2015Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PracticaRest2015Application.class)
@WebAppConfiguration
public class PracticaRest2015ApplicationTests {

	@Test
	public void whenArlinesRequestedThenResponseHasTwoAirlines() {
		RestTemplate rest = new RestTemplate();
		String url = "http://localhost:8080/airlines";
		List<String> airlines = rest.getForObject(url, List.class);
		assertEquals(airlines.size(), 2);
	}

}
