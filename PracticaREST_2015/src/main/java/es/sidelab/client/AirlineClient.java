package es.sidelab.client;

import java.util.List;

import org.springframework.web.client.RestTemplate;

public class AirlineClient {

	public static void main(String[] args) {
		
		RestTemplate rest = new RestTemplate();
		String url = "http://localhost:8080/airlines";
		List<String> airlines = rest.getForObject(url, List.class);
		
		System.out.println(airlines);
		
	}
}
