package es.sidelab;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.sidelab.model.Airline;
import es.sidelab.model.Route;

@RestController
public class AirlinesController {
	
	@Autowired
	AirlinesService service;

	@RequestMapping(value = "/airlines", method = RequestMethod.GET)
	public List<String> getAirlines() {
		return service.getAirlines();
	}
	
	@RequestMapping(value = "/{airlineId}", method = RequestMethod.GET)
	public List<String> getAirlineRoutes(@PathVariable String airlineId) {
		return service.getAirline(airlineId).getDestinations();
	}
	
	@RequestMapping(value = "/routes", method = RequestMethod.GET)
	public List<Route> getRoutes(@RequestParam String destination) {
		return service.getRoutes(destination);
	}
	
	@RequestMapping(value = "/{airlineId}/{routeId}", method = RequestMethod.GET)
	public Route getRoute(@PathVariable String airlineId, @PathVariable String routeId) {
		return service.getAirline(airlineId).getRoute(routeId);
	}
	
}
