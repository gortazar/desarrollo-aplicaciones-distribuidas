package es.sidelab.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Airline {
	
	public Airline(String uniqueName, Route... routes) {
		this.uniqueName = uniqueName;
		this.routes = Arrays.asList(routes);
		for(Route r : routes) {
			destinations.add(r.getDestination());
		}
	}
	
	String uniqueName;
	List<Route> routes;
	List<String> destinations = new ArrayList<String>();

	public String getUniqueName() {
		return uniqueName;
	}
	
	public List<Route> getRoutes() {
		return routes;
	}

	public List<String> getDestinations() {
		return destinations;
	}

	public Route getRoute(String routeId) {
		for(Route r : routes) {
			if(r.getId().equals(routeId)) {
				return r;
			}
		}
		return null;
	}
	
}
