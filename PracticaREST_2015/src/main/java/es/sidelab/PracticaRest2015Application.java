package es.sidelab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaRest2015Application {

    public static void main(String[] args) {
        SpringApplication.run(PracticaRest2015Application.class, args);
    }
}
