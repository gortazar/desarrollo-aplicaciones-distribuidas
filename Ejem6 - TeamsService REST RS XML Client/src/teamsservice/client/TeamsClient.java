package teamsservice.client;

import java.net.HttpURLConnection;
import java.net.URL;

import com.thoughtworks.xstream.XStream;

public class TeamsClient {

	public static void main(String[] args) throws Exception {

		// All Teams
		URL url = new URL("http://localhost:8888/teams");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.connect();

        XStream xstream = new XStream();
        xstream.alias("teamList", TeamList.class);
        xstream.alias("team", Team.class);
        xstream.alias("player", Player.class);
        xstream.addImplicitCollection(Team.class, "players", Player.class);
        xstream.addImplicitCollection(TeamList.class, "list", Team.class);
        xstream.autodetectAnnotations(true);

		TeamList teams = (TeamList)xstream.fromXML(conn.getInputStream());
		
        for (Team team : teams.getList()) {
            System.out.println("Team name: " + team.getName() +
                               " (roster count: " + team.getRosterCount() + ")");
            for (Player player : team.getPlayers())
                System.out.println("  Player: " + player.getNickname());
        }		

	}
}
