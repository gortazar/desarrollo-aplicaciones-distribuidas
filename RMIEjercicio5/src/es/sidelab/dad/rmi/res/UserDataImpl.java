package es.sidelab.dad.rmi.res;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class UserDataImpl implements UserData, Serializable {

	private String clientId;
	private List<TaskData> tasks = new ArrayList<TaskData>();
	
	public UserDataImpl(String clientId) {
		this.clientId = clientId;
	}

	public void setUsage(double seconds, double cost) {
		
		tasks.add(new TaskData(seconds, cost));
		
	}
	
	@Override
	public double getTotalCost() {
		
		double totalCost = 0.0;
		for(TaskData td : tasks) {
			totalCost += td.getCost(); 
		}
		return totalCost;
	}
	
}
