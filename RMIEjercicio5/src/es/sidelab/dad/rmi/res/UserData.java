package es.sidelab.dad.rmi.res;

public interface UserData {

	double getTotalCost();
	
}
