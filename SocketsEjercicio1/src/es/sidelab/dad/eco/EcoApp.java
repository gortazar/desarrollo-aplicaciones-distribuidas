package es.sidelab.dad.eco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class EcoApp {

	public static void main(String[] args) {
		
		new EcoApp().run();
		
	}
	
	public void run() {
		
		String host = "127.0.0.1";
		int port = 9999;
		try {
			Socket socket = new Socket(host, port);
			
			BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));
			BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter socketWriter = new PrintWriter(socket.getOutputStream());
			
			String line = stdinReader.readLine();
			while(!line.equals("x")) {

				socketWriter.println(line);
				socketWriter.flush();
				
				line = socketReader.readLine();
				System.out.println("eco>" + line);
				
				line = stdinReader.readLine();
				
			}
			
			// OJO!! No se cierran si salta alguna excepción antes de llegar aquí
			socketReader.close();
			socketWriter.close();
			socket.close();
			
		} catch (UnknownHostException e) {
			System.err.println("Host desconocido");
		} catch (IOException e) {
			System.err.println("Error de conexi�n: " + e.getMessage());
			e.printStackTrace();
		}
		
	}
}
