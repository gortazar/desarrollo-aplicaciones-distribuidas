package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EnlaceController {

	@RequestMapping("/enlace")
	public String enlace(@RequestParam(value = "num") String num, Model model) {

		model.addAttribute("num", num);

		return "enlace";
	}
}