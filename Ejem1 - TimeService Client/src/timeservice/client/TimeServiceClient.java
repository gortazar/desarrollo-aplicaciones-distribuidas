package timeservice.client;

import timeservice.client.ws.TimeService;
import timeservice.client.ws.TimeServiceService;


class TimeServiceClient {

	public static void main(String args[]) {
		
		TimeServiceService service = new TimeServiceService();
        TimeService timeService = service.getTimeServicePort();
		
		System.out.println(timeService.getTimeAsString());
		System.out.println(timeService.getTimeAsElapsed());
	}
}