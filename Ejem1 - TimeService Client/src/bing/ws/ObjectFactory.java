
package bing.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bing.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bing.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfPhonebookResult }
     * 
     */
    public ArrayOfPhonebookResult createArrayOfPhonebookResult() {
        return new ArrayOfPhonebookResult();
    }

    /**
     * Create an instance of {@link ImageResult }
     * 
     */
    public ImageResult createImageResult() {
        return new ImageResult();
    }

    /**
     * Create an instance of {@link ImageRequest }
     * 
     */
    public ImageRequest createImageRequest() {
        return new ImageRequest();
    }

    /**
     * Create an instance of {@link TranslationResult }
     * 
     */
    public TranslationResult createTranslationResult() {
        return new TranslationResult();
    }

    /**
     * Create an instance of {@link RelatedSearchResponse }
     * 
     */
    public RelatedSearchResponse createRelatedSearchResponse() {
        return new RelatedSearchResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSearchOption }
     * 
     */
    public ArrayOfSearchOption createArrayOfSearchOption() {
        return new ArrayOfSearchOption();
    }

    /**
     * Create an instance of {@link SearchRequest2 }
     * 
     */
    public SearchRequest2 createSearchRequest2() {
        return new SearchRequest2();
    }

    /**
     * Create an instance of {@link VideoRequest }
     * 
     */
    public VideoRequest createVideoRequest() {
        return new VideoRequest();
    }

    /**
     * Create an instance of {@link NewsResponse }
     * 
     */
    public NewsResponse createNewsResponse() {
        return new NewsResponse();
    }

    /**
     * Create an instance of {@link NewsRequest }
     * 
     */
    public NewsRequest createNewsRequest() {
        return new NewsRequest();
    }

    /**
     * Create an instance of {@link ImageResponse }
     * 
     */
    public ImageResponse createImageResponse() {
        return new ImageResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSourceType }
     * 
     */
    public ArrayOfSourceType createArrayOfSourceType() {
        return new ArrayOfSourceType();
    }

    /**
     * Create an instance of {@link DeepLink }
     * 
     */
    public DeepLink createDeepLink() {
        return new DeepLink();
    }

    /**
     * Create an instance of {@link ArrayOfWebSearchTag }
     * 
     */
    public ArrayOfWebSearchTag createArrayOfWebSearchTag() {
        return new ArrayOfWebSearchTag();
    }

    /**
     * Create an instance of {@link MobileWebResult }
     * 
     */
    public MobileWebResult createMobileWebResult() {
        return new MobileWebResult();
    }

    /**
     * Create an instance of {@link PhonebookRequest }
     * 
     */
    public PhonebookRequest createPhonebookRequest() {
        return new PhonebookRequest();
    }

    /**
     * Create an instance of {@link PhonebookResponse }
     * 
     */
    public PhonebookResponse createPhonebookResponse() {
        return new PhonebookResponse();
    }

    /**
     * Create an instance of {@link MobileWebResponse }
     * 
     */
    public MobileWebResponse createMobileWebResponse() {
        return new MobileWebResponse();
    }

    /**
     * Create an instance of {@link ArrayOfNewsResult }
     * 
     */
    public ArrayOfNewsResult createArrayOfNewsResult() {
        return new ArrayOfNewsResult();
    }

    /**
     * Create an instance of {@link TranslationRequest }
     * 
     */
    public TranslationRequest createTranslationRequest() {
        return new TranslationRequest();
    }

    /**
     * Create an instance of {@link ArrayOfMobileWebResult }
     * 
     */
    public ArrayOfMobileWebResult createArrayOfMobileWebResult() {
        return new ArrayOfMobileWebResult();
    }

    /**
     * Create an instance of {@link Query }
     * 
     */
    public Query createQuery() {
        return new Query();
    }

    /**
     * Create an instance of {@link WebSearchTag }
     * 
     */
    public WebSearchTag createWebSearchTag() {
        return new WebSearchTag();
    }

    /**
     * Create an instance of {@link Thumbnail }
     * 
     */
    public Thumbnail createThumbnail() {
        return new Thumbnail();
    }

    /**
     * Create an instance of {@link NewsCollection }
     * 
     */
    public NewsCollection createNewsCollection() {
        return new NewsCollection();
    }

    /**
     * Create an instance of {@link InstantAnswerResponse }
     * 
     */
    public InstantAnswerResponse createInstantAnswerResponse() {
        return new InstantAnswerResponse();
    }

    /**
     * Create an instance of {@link ArrayOfImageResult }
     * 
     */
    public ArrayOfImageResult createArrayOfImageResult() {
        return new ArrayOfImageResult();
    }

    /**
     * Create an instance of {@link SpellResult }
     * 
     */
    public SpellResult createSpellResult() {
        return new SpellResult();
    }

    /**
     * Create an instance of {@link SpellResponse }
     * 
     */
    public SpellResponse createSpellResponse() {
        return new SpellResponse();
    }

    /**
     * Create an instance of {@link WebResult }
     * 
     */
    public WebResult createWebResult() {
        return new WebResult();
    }

    /**
     * Create an instance of {@link ArrayOfError }
     * 
     */
    public ArrayOfError createArrayOfError() {
        return new ArrayOfError();
    }

    /**
     * Create an instance of {@link SearchResponse2 }
     * 
     */
    public SearchResponse2 createSearchResponse2() {
        return new SearchResponse2();
    }

    /**
     * Create an instance of {@link ArrayOfDeepLink }
     * 
     */
    public ArrayOfDeepLink createArrayOfDeepLink() {
        return new ArrayOfDeepLink();
    }

    /**
     * Create an instance of {@link ArrayOfWebResult }
     * 
     */
    public ArrayOfWebResult createArrayOfWebResult() {
        return new ArrayOfWebResult();
    }

    /**
     * Create an instance of {@link SearchRequest }
     * 
     */
    public SearchRequest createSearchRequest() {
        return new SearchRequest();
    }

    /**
     * Create an instance of {@link ArrayOfSpellResult }
     * 
     */
    public ArrayOfSpellResult createArrayOfSpellResult() {
        return new ArrayOfSpellResult();
    }

    /**
     * Create an instance of {@link PhonebookResult }
     * 
     */
    public PhonebookResult createPhonebookResult() {
        return new PhonebookResult();
    }

    /**
     * Create an instance of {@link ArrayOfTranslationResult }
     * 
     */
    public ArrayOfTranslationResult createArrayOfTranslationResult() {
        return new ArrayOfTranslationResult();
    }

    /**
     * Create an instance of {@link NewsArticle }
     * 
     */
    public NewsArticle createNewsArticle() {
        return new NewsArticle();
    }

    /**
     * Create an instance of {@link ArrayOfRelatedSearchResult }
     * 
     */
    public ArrayOfRelatedSearchResult createArrayOfRelatedSearchResult() {
        return new ArrayOfRelatedSearchResult();
    }

    /**
     * Create an instance of {@link VideoResponse }
     * 
     */
    public VideoResponse createVideoResponse() {
        return new VideoResponse();
    }

    /**
     * Create an instance of {@link WebRequest }
     * 
     */
    public WebRequest createWebRequest() {
        return new WebRequest();
    }

    /**
     * Create an instance of {@link ArrayOfNewsRelatedSearch }
     * 
     */
    public ArrayOfNewsRelatedSearch createArrayOfNewsRelatedSearch() {
        return new ArrayOfNewsRelatedSearch();
    }

    /**
     * Create an instance of {@link MobileWebRequest }
     * 
     */
    public MobileWebRequest createMobileWebRequest() {
        return new MobileWebRequest();
    }

    /**
     * Create an instance of {@link NewsResult }
     * 
     */
    public NewsResult createNewsResult() {
        return new NewsResult();
    }

    /**
     * Create an instance of {@link InstantAnswerResult }
     * 
     */
    public InstantAnswerResult createInstantAnswerResult() {
        return new InstantAnswerResult();
    }

    /**
     * Create an instance of {@link WebResponse }
     * 
     */
    public WebResponse createWebResponse() {
        return new WebResponse();
    }

    /**
     * Create an instance of {@link SearchResponse }
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link TranslationResponse }
     * 
     */
    public TranslationResponse createTranslationResponse() {
        return new TranslationResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMobileWebSearchOption }
     * 
     */
    public ArrayOfMobileWebSearchOption createArrayOfMobileWebSearchOption() {
        return new ArrayOfMobileWebSearchOption();
    }

    /**
     * Create an instance of {@link RelatedSearchResult }
     * 
     */
    public RelatedSearchResult createRelatedSearchResult() {
        return new RelatedSearchResult();
    }

    /**
     * Create an instance of {@link VideoResult }
     * 
     */
    public VideoResult createVideoResult() {
        return new VideoResult();
    }

    /**
     * Create an instance of {@link ArrayOfWebSearchOption }
     * 
     */
    public ArrayOfWebSearchOption createArrayOfWebSearchOption() {
        return new ArrayOfWebSearchOption();
    }

    /**
     * Create an instance of {@link ArrayOfInstantAnswerResult }
     * 
     */
    public ArrayOfInstantAnswerResult createArrayOfInstantAnswerResult() {
        return new ArrayOfInstantAnswerResult();
    }

    /**
     * Create an instance of {@link ArrayOfNewsCollection }
     * 
     */
    public ArrayOfNewsCollection createArrayOfNewsCollection() {
        return new ArrayOfNewsCollection();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link ArrayOfNewsArticle }
     * 
     */
    public ArrayOfNewsArticle createArrayOfNewsArticle() {
        return new ArrayOfNewsArticle();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link ArrayOfVideoResult }
     * 
     */
    public ArrayOfVideoResult createArrayOfVideoResult() {
        return new ArrayOfVideoResult();
    }

    /**
     * Create an instance of {@link NewsRelatedSearch }
     * 
     */
    public NewsRelatedSearch createNewsRelatedSearch() {
        return new NewsRelatedSearch();
    }

}
