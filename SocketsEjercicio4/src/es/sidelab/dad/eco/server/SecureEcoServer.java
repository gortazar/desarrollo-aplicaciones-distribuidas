package es.sidelab.dad.eco.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class SecureEcoServer {

	public static void main(String[] args) {

		try {

			int port = 9999;
			ServerSocketFactory serverSocketFactory = SSLServerSocketFactory.getDefault();
			SSLServerSocket serverSocket = (SSLServerSocket) serverSocketFactory.createServerSocket(port);

			while (true) {
				
				System.out.println("server> Esperando conexiones");
				Socket socket = serverSocket.accept();

				System.out.println("server> Nueva conexi�n aceptada");
				Thread thread = new Thread(new EcoProcess(socket));
				thread.start();
				
			}
			
		} catch (IOException e) {
			System.err.println("Problema en la conexi�n: " + e.getMessage());
		}

	}

}
