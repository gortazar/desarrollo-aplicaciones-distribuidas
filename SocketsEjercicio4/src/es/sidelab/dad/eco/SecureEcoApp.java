package es.sidelab.dad.eco;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SecureEcoApp {

	public static void main(String[] args) {
		
		new SecureEcoApp().run();
		
	}
	
	public void run() {
		
		String host = "127.0.0.1";
		int port = 9999;
		try {
			
			SocketFactory socketFactory = SSLSocketFactory.getDefault();
			SSLSocket socket = (SSLSocket) socketFactory.createSocket(host, port);
			
			BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));
			BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
			String line = stdinReader.readLine();
			while(!line.equals("x")) {

				socketWriter.write(line);
				socketWriter.newLine();
				socketWriter.flush();
				
				line = socketReader.readLine();
				System.out.println("eco>" + line);
				
				line = stdinReader.readLine();
				
			}
			
			socketReader.close();
			socketWriter.close();
			socket.close();
			
		} catch (UnknownHostException e) {
			System.err.println("Host desconocido");
		} catch (IOException e) {
			System.err.println("Error de conexi�n: " + e.getMessage());
		}
		
	}
}
