package es.sidelab.postealo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostealoTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostealoTestApplication.class, args);
    }
}
