package es.sidelab.postealo.test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static  org.junit.Assert.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = PostealoTestApplication.class)
public class PostealoTestApplicationTests {
	
	public static class Note {
		long id;
		String author;
		String content;
		List<String> tags = new ArrayList<String>();
		
		public Note() {
		}

		public Note(String author, String content, String... tags) {
			this.author = author;
			this.content = content;
			this.tags.addAll(Arrays.asList(tags));
		}

		public long getId() {
			return id;
		}
		
		public void setId(long id) {
			this.id = id;
		}
		
		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String text) {
			this.content = text;
		}

		public List<String> getTags() {
			return tags;
		}

		public void setTags(List<String> tags) {
			this.tags = tags;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((author == null) ? 0 : author.hashCode());
			result = prime * result
					+ ((content == null) ? 0 : content.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Note other = (Note) obj;
			if (author == null) {
				if (other.author != null)
					return false;
			} else if (!author.equals(other.author))
				return false;
			if (content == null) {
				if (other.content != null)
					return false;
			} else if (!content.equals(other.content))
				return false;
			return true;
		}
		
		
	}
	
	private static URI appuri;
	private static String PROPERTY_NAME = "appuri";

	@BeforeClass
	public static void setupBeforeClass() throws URISyntaxException {
		appuri = new URI(System.getProperty(PROPERTY_NAME));
	}

	private List<Note> notesAdded;
	
	@Before
	public void setup() {
		notesAdded = new ArrayList<PostealoTestApplicationTests.Note>();
	}
	
	@After
	public void tearDown() {
		RestTemplate restTemplate = new RestTemplate();
		Note[] notes = restTemplate.getForObject(appuri, Note[].class);
		
		for(Note note : notes) {
			if (notesAdded.contains(note)) {
				restTemplate.delete(appuri + "/" + note.id);
			}
		}
	}
	
	@Test
	public void POST() {
		
		Note note = new Note();
		note.author = "pepe";
		note.content = "Something to remember";
		note.tags.add("remember");
		note.tags.add("this");
		
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<Boolean> response = restTemplate.postForEntity(appuri, note, Boolean.class);
			assertTrue(response.getStatusCode() == HttpStatus.CREATED || response.getStatusCode() == HttpStatus.BAD_REQUEST);
			notesAdded.add(note);
		} catch(HttpClientErrorException e) {
			assertTrue(e.getStatusCode() == HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@Test
	public void GET() throws Exception {
		
		tryToAddNote("pepe", "Something to remember", "remember", "this");
		tryToAddNote("juan", "Something to forget about", "forget", "all");

		RestTemplate restTemplate = new RestTemplate();
		Note[] notes = restTemplate.getForObject(appuri, Note[].class);
		
		assertTrue(notes.length >= 2);
		List<Note> notesAsList = Arrays.asList(notes);
		assertTrue(notesAsList.contains(new Note("pepe", "Something to remember", "remember", "this")));
		assertTrue(notesAsList.contains(new Note("juan", "Something to forget about", "forget", "all")));
	}
	
	@Test
	public void GETExistentAuthor() throws Exception {

		tryToAddNote("pepe", "Something to remember", "remember", "this");
		tryToAddNote("juan", "Something to forget about", "forget", "all");
		
		RestTemplate restTemplate = new RestTemplate();
		Note[] notes = restTemplate.getForObject(appuri + "/juan", Note[].class);
		List<Note> notesAsList = Arrays.asList(notes);
		assertTrue(notesAsList.size() >= 1);
		assertTrue(notesAsList.contains(new Note("juan", "Something to forget about", "forget", "all")));

	}
	
	@Test
	public void DELETE() throws Exception {
		
		tryToAddNote("deleteme", "A note to delete", "delete");
		
		RestTemplate restTemplate = new RestTemplate();
		Note[] notes = restTemplate.getForObject(appuri + "/deleteme", Note[].class);
		
		List<Note> notesAsList = Arrays.asList(notes);
		assertTrue(notesAsList.contains(new Note("deleteme", "A note to delete", "delete")));
		
		int index = notesAsList.indexOf(new Note("deleteme", "A note to delete", "delete"));
		Note savedNote = notesAsList.get(index);
		
		restTemplate.delete(appuri + "/" + savedNote.id);
		
		Note[] newNotes = restTemplate.getForObject(appuri + "/deleteme", Note[].class);
		
		List<Note> newNotesAsList = Arrays.asList(newNotes);
		assertFalse(newNotesAsList.contains(new Note("deleteme", "A note to delete", "delete")));
		
	}
	
	public void tryToAddNote(String author, String text, String... tags) {

		Note note = new Note();
		note.author = author;
		note.content = text;
		note.tags.addAll(Arrays.asList(tags));		

		RestTemplate restTemplate = new RestTemplate();
		boolean accepted = false;
		int trials = 0;
		do
		try {
			ResponseEntity<Boolean> response = restTemplate.postForEntity(appuri, note, Boolean.class);
			assertTrue(response.getStatusCode() == HttpStatus.CREATED || response.getStatusCode() == HttpStatus.BAD_REQUEST);
			accepted = true;
			notesAdded.add(note);
		} catch(HttpClientErrorException e) {
			assertTrue(e.getStatusCode() == HttpStatus.BAD_REQUEST);
			trials++;
		}
		while(!accepted && trials < 100);

		if(!accepted) {
			throw new Error("Could not add note after 100 trials... aborting");
		}
	}
	

}
