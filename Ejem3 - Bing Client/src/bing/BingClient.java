package bing;

import bing.ws.ArrayOfSourceType;
import bing.ws.LiveSearchPortType;
import bing.ws.LiveSearchService;
import bing.ws.SearchRequest;
import bing.ws.SearchRequest2;
import bing.ws.SearchResponse;
import bing.ws.SourceType;
import bing.ws.WebResponse;
import bing.ws.WebResult;

public class BingClient {

	public static void main(String[] args) {
		
		LiveSearchService service = new LiveSearchService();		
		LiveSearchPortType port = service.getLiveSearchPort();
		
		SearchRequest2 sr2 = new SearchRequest2(); 
		sr2.setQuery("Web Services in Java");
		sr2.setAppId("76FFEE4549001C4BDD01F9A213C63B337F2CAA56");
		ArrayOfSourceType sources = new ArrayOfSourceType();
		sources.getSourceType().add(SourceType.WEB);
		sr2.setSources(sources);
		
		SearchRequest sr = new SearchRequest();
		sr.setParameters(sr2);
		SearchResponse sResp = port.search(sr);
		
		WebResponse response = sResp.getParameters().getWeb();
		for(WebResult result : response.getResults().getWebResult()){
			
			System.out.println("Title: "+result.getTitle());
			System.out.println("Description: "+result.getDescription());
			System.out.println("URL: "+result.getUrl());
			System.out.println("Display URL: "+result.getDisplayUrl());
			System.out.println("DateTime: "+result.getDateTime());
			System.out.println();
			
		}
		
	}
	
}
