package es.sidelab.dad.eco.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EcoServerPool {

	public static void main(String[] args) {

		try {

			int port = 9999;
			ServerSocket serverSocket = new ServerSocket(port);

			ExecutorService executorService = Executors.newFixedThreadPool(100);
			
			while (true) {
				
				System.out.println("server> Esperando conexiones");
				Socket socket = serverSocket.accept();

				System.out.println("server> Nueva conexión aceptada");
				executorService.execute(new EcoProcess(socket));
				
			}
			
		} catch (IOException e) {
			System.err.println("Problema en la conexión: " + e.getMessage());
		}

	}

}
