package es.sidelab.dad.eco.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class EcoProcess implements Runnable {

	private Socket socket;

	public EcoProcess(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {

		try {
			BufferedReader socketReader = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
			PrintWriter socketWriter = new PrintWriter(socket.getOutputStream());

			String line;
			while ((line = socketReader.readLine()) != null) {

				socketWriter.println(line);
				socketWriter.flush();

			}
			
			socketReader.close();
			socketWriter.close();
			socket.close();

		} catch (IOException e) {
			System.err.println("Problema en la conexi�n: " + e.getMessage());
		}

	}

}
