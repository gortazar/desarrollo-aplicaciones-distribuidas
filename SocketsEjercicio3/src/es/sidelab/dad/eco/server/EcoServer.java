package es.sidelab.dad.eco.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class EcoServer {

	public static void main(String[] args) {

		try {

			int port = 9999;
			ServerSocket serverSocket = new ServerSocket(port);

			while (true) {
				
				System.out.println("server> Esperando conexiones");
				Socket socket = serverSocket.accept();

				System.out.println("server> Nueva conexión aceptada");
				Thread thread = new Thread(new EcoProcess(socket));
				thread.start();
				
			}
			
		} catch (IOException e) {
			System.err.println("Problema en la conexión: " + e.getMessage());
		}

	}

}
