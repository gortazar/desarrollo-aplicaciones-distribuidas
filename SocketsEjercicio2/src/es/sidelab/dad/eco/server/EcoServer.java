package es.sidelab.dad.eco.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EcoServer {

	public static void main(String[] args) {

		new EcoServer().run();

	}

	public void run() {

		int port = 9999;
		try {

			ServerSocket serverSocket = new ServerSocket(port);

			while (true) {

				System.out.println("server> Esperando conexiones");
				Socket socket = serverSocket.accept();

				System.out.println("server> Conexi�n aceptada");

				BufferedReader socketReader = new BufferedReader(
						new InputStreamReader(socket.getInputStream()));
				PrintWriter socketWriter = new PrintWriter(socket.getOutputStream());

				String line;
				while ((line = socketReader.readLine()) != null) {

					System.out.println("server> " + line);
					socketWriter.println(line);
					socketWriter.flush();
					System.out.println("server> Waiting...");
				}

			}

		} catch (IOException e) {
			System.err.println("Problema en la conexi�n: " + e.getMessage());
		}

	}

}
