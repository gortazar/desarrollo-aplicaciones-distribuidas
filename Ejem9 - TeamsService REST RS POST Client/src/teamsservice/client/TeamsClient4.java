package teamsservice.client;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

import com.thoughtworks.xstream.XStream;

public class TeamsClient4 {

	public static void main(String[] args) throws Exception {

		// All Teams by POST
		URL url = new URL("http://localhost:8888/teamjson2");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true); // Triggers POST.
		conn.addRequestProperty("Content-Type", "application/json");
		conn.addRequestProperty("Accept", "application/json");

		TeamRequest req = new TeamRequest("Marx Brothers");

		ObjectMapper mapper = new ObjectMapper();
		mapper.getDeserializationConfig().setAnnotationIntrospector(
				new JaxbAnnotationIntrospector());

		OutputStream output = conn.getOutputStream();
		mapper.writeValue(output, req);
		output.close();

		// conn.connect();
		Team team = mapper.readValue(conn.getInputStream(), Team.class);

		System.out.println("Team name: " + team.getName() + " (roster count: "
				+ team.getRosterCount() + ")");
		for (Player player : team.getPlayers())
			System.out.println("  Player: " + player.getNickname());

	}
}
