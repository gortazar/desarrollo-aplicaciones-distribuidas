package teamsservice.client;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TeamRequest {

	private String name;
	
	public TeamRequest() {
	}
	
	public TeamRequest(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
