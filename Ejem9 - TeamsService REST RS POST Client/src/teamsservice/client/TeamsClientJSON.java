package teamsservice.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TeamsClientJSON {

	public static void main(String[] args) throws Exception {

		// All Teams
		URL url = new URL("http://localhost:8888/teams");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Accept", "application/json");
		conn.connect();

        String jsonResponse = buildString(conn.getInputStream());

		System.out.println(jsonResponse);		

	}

	private static String buildString(InputStream inputStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder sb = new StringBuilder();
		String line = reader.readLine();
		while(line != null){
			sb.append(line).append("\r\n");
			line = reader.readLine();
		}
		return sb.toString();
	}
}
