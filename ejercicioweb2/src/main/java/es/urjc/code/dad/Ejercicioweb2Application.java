package es.urjc.code.dad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Ejercicioweb2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejercicioweb2Application.class, args);
	}
}
