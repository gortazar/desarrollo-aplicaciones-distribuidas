package tema3;

import java.rmi.*;
import java.rmi.registry.*;

public class Cliente {

	public static void main(String[] args) {
		try{
			Registry registry=LocateRegistry.getRegistry();
			Hello hello=(Hello)registry.lookup("hello");
			String saludo=hello.hello();
			System.out.println(saludo);
		}
		catch(RemoteException e){
			System.err.println("Algo falla: "+e.getMessage());
		}
		catch(NotBoundException e){
			System.err.println("Algo falla al resolver el nombre: "+e.getMessage());
		}
	}
}