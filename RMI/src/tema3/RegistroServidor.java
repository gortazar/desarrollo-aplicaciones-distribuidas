package tema3;

import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

public class RegistroServidor {

	public static void main(String[] args) {
		HelloImpl hello=new HelloImpl();
		try{
			Hello stub=(Hello) UnicastRemoteObject.exportObject(hello,0);
			Registry registry=LocateRegistry.getRegistry();
			registry.bind("hello",stub);
			System.out.println("Hecho!!");
		}
		catch(RemoteException e){
			//System.err.println("Algo ha ido mal: "+e.getMessage());
			e.printStackTrace();
		}
		catch(AlreadyBoundException e){
			System.err.println("El nombre ya ha sido asociado a un objeto: "+e.getMessage());
		}
	}
}
