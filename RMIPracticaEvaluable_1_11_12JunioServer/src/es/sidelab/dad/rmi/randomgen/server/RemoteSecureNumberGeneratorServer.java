package es.sidelab.dad.rmi.randomgen.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import es.sidelab.dad.rmi.randomgen.RandomNumberGeneratorService;
import es.sidelab.dad.rmi.randomgen.impl.RandomNumberGeneratorServiceImpl;

public class RemoteSecureNumberGeneratorServer {

	public static void main(String[] args) {

		System.setSecurityManager(new RMISecurityManager());
		
		RandomNumberGeneratorServiceImpl serviceImpl = new RandomNumberGeneratorServiceImpl();
		
		try {
			
			RandomNumberGeneratorService service = (RandomNumberGeneratorService) UnicastRemoteObject.exportObject(serviceImpl, 8899, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());

			Registry registry = LocateRegistry.getRegistry();
			registry.bind("rangen", service);
			
			System.out.println("Server ready...");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
	}
	
}
