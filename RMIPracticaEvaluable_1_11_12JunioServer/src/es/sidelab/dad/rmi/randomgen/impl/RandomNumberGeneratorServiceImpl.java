package es.sidelab.dad.rmi.randomgen.impl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import es.sidelab.dad.rmi.randomgen.RandomNumberGeneratorService;

public class RandomNumberGeneratorServiceImpl implements RandomNumberGeneratorService {

	private static final long SEED = 0;
	
	private Map<String, Random> randomGenerators = new HashMap<String, Random>();
	
	@Override
	public List<Integer> generateIntegerRandomSequence(int sequenceLength, String clientId) throws RemoteException {
		Random random = createRandomIfNecessary(clientId);
		
		List<Integer> sequence = new ArrayList<Integer>();
		for(int i = 0; i < sequenceLength; i++) {
			sequence.add(random.nextInt());
		}
		return sequence;
	}

	private Random createRandomIfNecessary(String clientId) {
		if(randomGenerators.containsKey(clientId)) {
			return randomGenerators.get(clientId);
		}
		Random clientRandom = new Random(SEED);
		randomGenerators.put(clientId, clientRandom);
		return clientRandom;
	}

	@Override
	public List<Double> generateDoubleRandomSequence(int sequenceLength, String clientId) throws RemoteException {
		Random random = createRandomIfNecessary(clientId);
		
		List<Double> sequence = new ArrayList<Double>();
		for(int i = 0; i < sequenceLength; i++) {
			sequence.add(random.nextDouble());
		}
		return sequence;
	}

}
