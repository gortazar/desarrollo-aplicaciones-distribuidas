package es.sidelab.dad.rmi.res;

import java.io.Serializable;

public class TaskData implements Serializable {

	private double seconds;
	private double cost;

	public TaskData(double seconds, double cost) {
		this.seconds = seconds;
		this.cost = cost;
	}
	
	public double getSeconds() {
		return seconds;
	}
	
	public double getCost() {
		return cost;
	}
}
