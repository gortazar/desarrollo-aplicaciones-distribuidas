package es.sidelab.dad.rmi.res.sum;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import es.sidelab.dad.rmi.res.RemoteComputingService;
import es.sidelab.dad.rmi.res.RemoteTaskExecutor;
import es.sidelab.dad.rmi.res.UserData;

public class SumClient {

	public static void main(String[] args) {
		
		try {
			
			Registry registry = LocateRegistry.getRegistry();
			RemoteComputingService res = (RemoteComputingService) registry.lookup("rtask");
			
			SumTask st = new SumTask(new int[]{1,2,3,4,5,6,7,8,9,10});
			
			RemoteTaskExecutor rte = res.createTaskExecutor("yo");
			NumTaskResult numResult = (NumTaskResult) rte.run(st);
			System.out.println(numResult.getValue());
			
			UserData userData = res.getRemoteServiceUsage("yo");
			System.out.println(userData.getTotalCost());
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}
}
