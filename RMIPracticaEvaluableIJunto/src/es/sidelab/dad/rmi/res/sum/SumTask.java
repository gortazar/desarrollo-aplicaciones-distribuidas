package es.sidelab.dad.rmi.res.sum;

import java.io.Serializable;
import java.rmi.RemoteException;

import es.sidelab.dad.rmi.res.Task;
import es.sidelab.dad.rmi.res.TaskResult;

public class SumTask implements Task, Serializable {

	private static final long serialVersionUID = -1200281396040626552L;
	
	private int[] values;
	private long result;

	public SumTask(int[] values) {
		this.values = values;
	}
	
	@Override
	public void run() throws RemoteException {

		long total = 0;
		for(int v : values) {
			total += v;
		}

		result = total;
	}

	@Override
	public TaskResult getResult() throws RemoteException {
		
		return new NumTaskResult(result);
	}

}
