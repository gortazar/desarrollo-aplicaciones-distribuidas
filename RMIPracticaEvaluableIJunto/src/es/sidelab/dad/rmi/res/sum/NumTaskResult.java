package es.sidelab.dad.rmi.res.sum;

import java.io.Serializable;

import es.sidelab.dad.rmi.res.TaskResult;

public class NumTaskResult implements TaskResult, Serializable {

	private static final long serialVersionUID = 3055419858565979533L;
	
	private double value;

	public NumTaskResult(double value) {
		this.value = value;
	}
	
	public double getValue() {
		return value;
	}
}
