package es.sidelab.dad.rmi.res;

import java.io.Serializable;
import java.util.List;

public class Report implements Serializable {

	private List<UserData> data;

	public Report(List<UserData> data) {
		this.data = data;
	}
}
