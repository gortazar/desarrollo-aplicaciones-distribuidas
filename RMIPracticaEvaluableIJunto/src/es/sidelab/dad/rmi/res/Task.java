package es.sidelab.dad.rmi.res;

import java.rmi.RemoteException;

public interface Task {

	public void run() throws RemoteException;
	public TaskResult getResult() throws RemoteException;
	
}
