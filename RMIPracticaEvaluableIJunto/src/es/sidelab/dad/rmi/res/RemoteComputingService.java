package es.sidelab.dad.rmi.res;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteComputingService extends Remote {
	
	RemoteTaskExecutor createTaskExecutor(String clientId) throws RemoteException;
	UserData getRemoteServiceUsage(String clientId) throws RemoteException;
	Report getReport() throws RemoteException;

}
