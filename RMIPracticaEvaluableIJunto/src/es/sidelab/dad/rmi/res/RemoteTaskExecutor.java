package es.sidelab.dad.rmi.res;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteTaskExecutor extends Remote {
	
	public TaskResult run(Task t) throws RemoteException;

}
