package es.sidelab.dad.rmi.res.server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import es.sidelab.dad.rmi.res.RemoteTaskExecutor;
import es.sidelab.dad.rmi.res.Task;
import es.sidelab.dad.rmi.res.TaskResult;
import es.sidelab.dad.rmi.res.UserDataImpl;

public class RemoteTaskExecutorImpl extends UnicastRemoteObject implements RemoteTaskExecutor, Serializable {

	private String clientId;
	private UserDataImpl userData;

	public RemoteTaskExecutorImpl(String clientId,
			UserDataImpl data) throws RemoteException{
		super();
		this.clientId = clientId;
		this.userData = data;
	}

	@Override
	public TaskResult run(Task t) throws RemoteException {
		
		long timeStart = System.currentTimeMillis();
		t.run();
		long timeEnd = System.currentTimeMillis();
		
		double seconds = (timeEnd - timeStart) / 1000.0;
		double cost = seconds * 0.5;
		userData.setUsage(seconds, cost);
		
		return t.getResult();
	}

}
