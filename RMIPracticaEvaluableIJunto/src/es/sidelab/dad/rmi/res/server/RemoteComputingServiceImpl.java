package es.sidelab.dad.rmi.res.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.sidelab.dad.rmi.res.RemoteComputingService;
import es.sidelab.dad.rmi.res.RemoteTaskExecutor;
import es.sidelab.dad.rmi.res.Report;
import es.sidelab.dad.rmi.res.UserData;
import es.sidelab.dad.rmi.res.UserDataImpl;

public class RemoteComputingServiceImpl implements RemoteComputingService {

	Map<String, UserData> clientUsage = new HashMap<String, UserData>(); 
	
	@Override
	public RemoteTaskExecutor createTaskExecutor(String clientId) throws RemoteException {
		
		UserDataImpl data = null;
		if(clientUsage.containsKey(clientId)) {
			data = (UserDataImpl) clientUsage.get(clientId);
		} else {
			data = new UserDataImpl(clientId);
			clientUsage.put(clientId, data);
		}
		
		return new RemoteTaskExecutorImpl(clientId, data);
	}

	@Override
	public UserData getRemoteServiceUsage(String clientId) {
		
		return clientUsage.get(clientId);
		
	}

	@Override
	public Report getReport() {
		
		return new Report(new ArrayList<UserData>(clientUsage.values()));
		
	}

}
