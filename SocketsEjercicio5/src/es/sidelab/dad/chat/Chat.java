package es.sidelab.dad.chat;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.UIManager;

public class Chat extends AbstractChat {

  private Socket socket;
  private ObjectOutputStream oos;
  private ObjectInputStream ois;
  private String nombreOtroContertulio;  
  private ServerSocket ss;

  boolean esperaCancelada = false;

  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager
          .getSystemLookAndFeelClassName());
    } catch (Exception e) {
    }

    new Chat();
  }

  @Override
  protected void enviar(String frase) throws Exception {
    MensajeFrase mf = new MensajeFrase(frase);
    oos.writeObject(mf);
  }

  @Override
  protected void cerrarConexion() throws Exception {
    MensajeFin mf = new MensajeFin();
    oos.writeObject(mf);
    oos.flush();
  }

  private void conexionCerradaContertulio() {
    try {
      oos.close();
      ois.close();
      socket.close();
    } catch (Exception e) {
      error(e);
    }
    conexionCerrada();
  }

  @Override
  protected void conectar(String host, int puerto, String nombre)
      throws Exception {
    socket = new Socket(host, puerto);

    MensajeInicio mi = new MensajeInicio(nombre);
    oos = new ObjectOutputStream(socket.getOutputStream());
    oos.writeObject(mi);

    ois = new ObjectInputStream(socket.getInputStream());
    MensajeInicio miServidor = (MensajeInicio) ois.readObject();
    this.nombreOtroContertulio = miServidor.getNombre();

    new Thread(new Runnable(){
      public void run() {
        conexionAbierta();
        try {
          recibirMensajes();
        } catch (Exception e) {
          error(e);
        }
      }
    }).start();
    
  }

  @Override
  protected void esperarConexion(final int puerto, final String nombre)
      throws Exception {

    new Thread(new Runnable(){
      
      public void run() {
        esperandoConexion();

        try {
          
          ss = new ServerSocket(puerto);
          socket = ss.accept();
          ss.close();
          
          ois = new ObjectInputStream(socket.getInputStream());
          MensajeInicio miServidor = (MensajeInicio) ois
              .readObject();
          nombreOtroContertulio = miServidor.getNombre();

          MensajeInicio mi = new MensajeInicio(nombre);
          oos = new ObjectOutputStream(socket.getOutputStream());
          oos.writeObject(mi);

          conexionAbierta();
          recibirMensajes();

        } catch (SocketException e){
          //Al cancelar el accept, salta una SocketException
          if(!esperaCancelada){
            error(e);
          } else {
            esperaCancelada = false;
          }
        } catch (Exception e) {
          error(e);
        }
        
      }
    }).start();
      
  }

  private void recibirMensajes() throws Exception {
    while (true) {
      Mensaje m = (Mensaje) ois.readObject();
      if (m instanceof MensajeFrase) {
        MensajeFrase mf = (MensajeFrase) m;
        insertarFrase(this.nombreOtroContertulio, mf.getFrase());
      } else if (m instanceof MensajeFin) {
        cerrarConexion();
        conexionCerradaContertulio();
        break;
      }
    }
  }

  @Override
  protected void esperaCancelada() {
    esperaCancelada = true;
    try {
      ss.close();
    } catch (IOException e) {
      error(e);
    }
  }

}
