package es.sidelab.dad.chat;

public class MensajeFrase extends Mensaje {

	String frase;

	public MensajeFrase(String frase) {
		this.frase = frase;
	}

	public String getFrase() {
		return frase;
	}

	public void setFrase(String frase) {
		this.frase = frase;
	}

	
}
