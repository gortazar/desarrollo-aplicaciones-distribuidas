package es.sidelab.dad.chat;

public class MensajeInicio extends Mensaje {

	String nombre;

	public MensajeInicio(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
		
}
