package es.sidelab.dad.chat;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JLabel;

public abstract class AbstractChat extends JFrame {

  private JPanel jContentPane = null;

  private JPanel panelConversacion = null;

  private JPanel panelCliente = null;

  private JScrollPane jScrollPane = null;

  private JTextArea jTextArea = null;

  private JPanel panelControlConversacion = null;

  private JTextField textoConversacion = null;

  private JButton botonEnviar = null;

  private JButton botonCerrarConexion = null;

  private JPanel panelConexiones = null;

  private JLabel jLIPServidor = null;

  private JLabel jLIPServidorC = null;

  private JTextField jTFHostC = null;

  private JTextField jTFPuertoC = null;

  private JButton jBConectar = null;

  private JLabel jPuertoServidorS = null;

  private JLabel jLabel = null;

  private JLabel jLabel1 = null;

  private JPanel jPServidor = null;

  private JLabel jLPuertoServidorS = null;

  private JTextField jTFPuertoServidorS = null;

  private JButton jBEsperarConexion = null;

  private JPanel jPanelNombre = null;

  private JLabel jLNombre = null;

  private JTextField jTFNombre = null;

  private JDialog dialogo;

  /**
   * This is the default constructor
   */
  public AbstractChat() {
    super();
    initialize();
  }

  /**
   * This method initializes this
   * 
   * @return void
   */
  private void initialize() {
    this.setSize(500, 500);
    this
        .setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
    this.setContentPane(getJContentPane());
    this.setTitle("SuperChat");
    this.conexionCerrada();
    this.setVisible(true);
  }

  /**
   * This method initializes jContentPane
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJContentPane() {
    if (jContentPane == null) {
      GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
      gridBagConstraints13.gridx = 0;
      gridBagConstraints13.fill = java.awt.GridBagConstraints.BOTH;
      gridBagConstraints13.gridy = 0;
      GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
      gridBagConstraints12.gridx = 0;
      gridBagConstraints12.ipadx = 167;
      gridBagConstraints12.fill = java.awt.GridBagConstraints.HORIZONTAL;
      gridBagConstraints12.gridy = 1;
      GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
      gridBagConstraints11.gridx = 0;
      gridBagConstraints11.ipadx = 242;
      gridBagConstraints11.ipady = 243;
      gridBagConstraints11.weightx = 1.0D;
      gridBagConstraints11.weighty = 1.0D;
      gridBagConstraints11.fill = java.awt.GridBagConstraints.BOTH;
      gridBagConstraints11.gridy = 2;
      jContentPane = new JPanel();
      jContentPane.setLayout(new GridBagLayout());
      jContentPane.setBorder(javax.swing.BorderFactory
          .createEmptyBorder(5, 5, 5, 5));
      jContentPane.add(getPanelConversacion(),
          gridBagConstraints11);
      jContentPane.add(getPanelConexiones(),
          gridBagConstraints12);
      jContentPane.add(getJPanelNombre(), gridBagConstraints13);
    }
    return jContentPane;
  }

  /**
   * This method initializes panelConversacion
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getPanelConversacion() {
    if (panelConversacion == null) {
      BorderLayout borderLayout = new BorderLayout();
      borderLayout.setHgap(5);
      borderLayout.setVgap(5);
      panelConversacion = new JPanel();
      panelConversacion.setLayout(borderLayout);
      panelConversacion
          .setBorder(javax.swing.BorderFactory
              .createCompoundBorder(
                  javax.swing.BorderFactory
                      .createTitledBorder(
                          null,
                          "Conversaci�n",
                          javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                          javax.swing.border.TitledBorder.DEFAULT_POSITION,
                          null, null), javax.swing.BorderFactory
                      .createEmptyBorder(5, 5, 5, 5)));
      panelConversacion.add(getJScrollPane(),
          java.awt.BorderLayout.CENTER);
      panelConversacion.add(getPanelControlConversacion(),
          java.awt.BorderLayout.SOUTH);
    }
    return panelConversacion;
  }

  /**
   * This method initializes panelConexion
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getPanelConexion() {
    if (panelCliente == null) {
      GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
      gridBagConstraints10.gridx = 0;
      gridBagConstraints10.gridwidth = 2;
      gridBagConstraints10.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints10.gridy = 2;
      GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
      gridBagConstraints9.fill = java.awt.GridBagConstraints.HORIZONTAL;
      gridBagConstraints9.gridy = 1;
      gridBagConstraints9.weightx = 1.0;
      gridBagConstraints9.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints9.gridx = 1;
      GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
      gridBagConstraints8.fill = java.awt.GridBagConstraints.HORIZONTAL;
      gridBagConstraints8.gridy = 0;
      gridBagConstraints8.weightx = 1.0;
      gridBagConstraints8.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints8.gridx = 1;
      GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
      gridBagConstraints7.gridx = 0;
      gridBagConstraints7.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints7.gridy = 1;
      jLIPServidorC = new JLabel();
      jLIPServidorC.setText("Puerto");
      GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
      gridBagConstraints6.gridx = 0;
      gridBagConstraints6.gridy = 0;
      jLIPServidor = new JLabel();
      jLIPServidor.setText("Host");
      panelCliente = new JPanel();
      panelCliente.setLayout(new GridBagLayout());
      panelCliente.setBorder(javax.swing.BorderFactory
          .createTitledBorder("Cliente"));
      panelCliente.add(jLIPServidor, gridBagConstraints6);
      panelCliente.add(jLIPServidorC, gridBagConstraints7);
      panelCliente.add(getJTFHostC(), gridBagConstraints8);
      panelCliente.add(getJTFPuertoC(), gridBagConstraints9);
      panelCliente.add(getJBConectar(), gridBagConstraints10);
    }
    return panelCliente;
  }

  /**
   * This method initializes jScrollPane
   * 
   * @return javax.swing.JScrollPane
   */
  private JScrollPane getJScrollPane() {
    if (jScrollPane == null) {
      jScrollPane = new JScrollPane();
      jScrollPane.setViewportView(getJTextArea());
    }
    return jScrollPane;
  }

  /**
   * This method initializes jTextArea
   * 
   * @return javax.swing.JTextArea
   */
  private JTextArea getJTextArea() {
    if (jTextArea == null) {
      jTextArea = new JTextArea();
      jTextArea.setEditable(false);
    }
    return jTextArea;
  }

  /**
   * This method initializes panelControlConversacion
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getPanelControlConversacion() {
    if (panelControlConversacion == null) {
      GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
      gridBagConstraints2.insets = new java.awt.Insets(5, 5, 5,
          0);
      gridBagConstraints2.gridy = 0;
      gridBagConstraints2.gridx = 2;
      GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
      gridBagConstraints1.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints1.gridy = 0;
      gridBagConstraints1.gridx = 1;
      GridBagConstraints gridBagConstraints = new GridBagConstraints();
      gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
      gridBagConstraints.gridx = 0;
      gridBagConstraints.gridy = 0;
      gridBagConstraints.weightx = 1.0;
      gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 5);
      panelControlConversacion = new JPanel();
      panelControlConversacion.setLayout(new GridBagLayout());
      panelControlConversacion.add(getTextoConversacion(),
          gridBagConstraints);
      panelControlConversacion.add(getBotonEnviar(),
          gridBagConstraints1);
      panelControlConversacion.add(getBotonCerrarConexion(),
          gridBagConstraints2);
    }
    return panelControlConversacion;
  }

  /**
   * This method initializes textoConversacion
   * 
   * @return javax.swing.JTextField
   */
  private JTextField getTextoConversacion() {
    if (textoConversacion == null) {
      textoConversacion = new JTextField();
      textoConversacion
          .addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(
                java.awt.event.ActionEvent e) {
              enviarAction();
            }
          });
    }
    return textoConversacion;
  }

  /**
   * This method initializes botonEnviar
   * 
   * @return javax.swing.JButton
   */
  private JButton getBotonEnviar() {
    if (botonEnviar == null) {
      botonEnviar = new JButton();
      botonEnviar.setText("Enviar");
      botonEnviar
          .addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(
                java.awt.event.ActionEvent e) {
              enviarAction();
            }
          });
    }
    return botonEnviar;
  }

  protected void enviarAction() {
    String frase = textoConversacion.getText();
    textoConversacion.setText("");
    insertarFrase(jTFNombre.getText(), frase);
    try {
      enviar(frase);
    } catch (Exception ex) {
      error(ex);
    }
  }

  protected void insertarFrase(final String nombre,
      final String frase) {
    if (SwingUtilities.isEventDispatchThread()) {
      insertarFraseEDT(nombre, frase);
    } else {
      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          insertarFraseEDT(nombre, frase);
        }
      });
    }
  }

  private void insertarFraseEDT(String nombre, String frase) {
    jTextArea.append(nombre + " > " + frase + "\n");
  }

  protected abstract void enviar(String frase) throws Exception;

  /**
   * This method initializes botonCerrarConexion
   * 
   * @return javax.swing.JButton
   */
  private JButton getBotonCerrarConexion() {
    if (botonCerrarConexion == null) {
      botonCerrarConexion = new JButton();
      botonCerrarConexion.setText("Cerrar Conexi�n");
      botonCerrarConexion
          .addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(
                java.awt.event.ActionEvent e) {
              try {
                cerrarConexion();
              } catch (Exception ex) {
                error(ex);
              }
            }
          });
    }
    return botonCerrarConexion;
  }

  protected void conexionCerrada() {
    if (SwingUtilities.isEventDispatchThread()) {
      conexionCerradaEDT();
    } else {
      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          conexionCerradaEDT();
        }
      });
    }
  }

  protected void conexionCerradaEDT() {
    textoConversacion.setEnabled(false);
    botonEnviar.setEnabled(false);
    botonCerrarConexion.setEnabled(false);
    jTFHostC.setEnabled(true);
    jTFPuertoC.setEnabled(true);
    jBConectar.setEnabled(true);
    jTFPuertoServidorS.setEnabled(true);
    jBEsperarConexion.setEnabled(true);

    jTextArea.setText("");
  }

  protected void conexionAbierta() {
    
    try {
      SwingUtilities.invokeAndWait(new Runnable(){
        public void run() {
          if(dialogo != null && dialogo.isVisible()){
            dialogo.setVisible(false);
          }
          
          textoConversacion.setEnabled(true);
          botonEnviar.setEnabled(true);
          botonCerrarConexion.setEnabled(true);
          jTFHostC.setEnabled(false);
          jTFPuertoC.setEnabled(false);
          jBConectar.setEnabled(false);
          jTFPuertoServidorS.setEnabled(false);
          jBEsperarConexion.setEnabled(false);        
        }});
    } catch (InterruptedException e) {
      error(e);
    } catch (InvocationTargetException e) {
      error(e);
    }   
    
  }

  protected abstract void cerrarConexion() throws Exception;

  /**
   * This method initializes panelConexiones
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getPanelConexiones() {
    if (panelConexiones == null) {
      GridLayout gridLayout = new GridLayout();
      gridLayout.setRows(1);
      gridLayout.setHgap(5);
      gridLayout.setColumns(2);
      panelConexiones = new JPanel();
      panelConexiones.setLayout(gridLayout);
      panelConexiones.add(getPanelConexion(), null);
      panelConexiones.add(getJPServidor(), null);
    }
    return panelConexiones;
  }

  /**
   * This method initializes jTFHostC
   * 
   * @return javax.swing.JTextField
   */
  private JTextField getJTFHostC() {
    if (jTFHostC == null) {
      jTFHostC = new JTextField();
      jTFHostC.setText("127.0.0.1");
    }
    return jTFHostC;
  }

  /**
   * This method initializes jTFPuertoC
   * 
   * @return javax.swing.JTextField
   */
  private JTextField getJTFPuertoC() {
    if (jTFPuertoC == null) {
      jTFPuertoC = new JTextField();
      jTFPuertoC.setText("2222");
    }
    return jTFPuertoC;
  }

  /**
   * This method initializes jBConectar
   * 
   * @return javax.swing.JButton
   */
  private JButton getJBConectar() {
    if (jBConectar == null) {
      jBConectar = new JButton();
      jBConectar.setText("Conectar");
      jBConectar
          .addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(
                java.awt.event.ActionEvent e) {
              conectarAction();
            }
          });
    }
    return jBConectar;
  }

  private void conectarAction() {
    try {
      int puerto = Integer.parseInt(jTFPuertoC.getText());
      String host = jTFHostC.getText();
      String nombre = jTFNombre.getText();
      conectar(host, puerto, nombre);
    } catch (Exception ex) {
      JOptionPane.showMessageDialog(this, "Error: "
          + ex.getLocalizedMessage());
    }
  }

  protected abstract void conectar(String host, int puerto,
      String nombre) throws Exception;

  /**
   * This method initializes jPServidor
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPServidor() {
    if (jPServidor == null) {
      GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
      gridBagConstraints5.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints5.gridy = 1;
      gridBagConstraints5.gridwidth = 2;
      gridBagConstraints5.gridx = 0;
      GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
      gridBagConstraints4.fill = java.awt.GridBagConstraints.HORIZONTAL;
      gridBagConstraints4.gridx = 1;
      gridBagConstraints4.gridy = 0;
      gridBagConstraints4.weightx = 1.0;
      gridBagConstraints4.insets = new java.awt.Insets(5, 5, 5,
          5);
      GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
      gridBagConstraints3.insets = new java.awt.Insets(5, 5, 5,
          5);
      gridBagConstraints3.gridy = 0;
      gridBagConstraints3.gridx = 0;
      jLPuertoServidorS = new JLabel();
      jLPuertoServidorS.setText("Puerto");
      jPServidor = new JPanel();
      jPServidor.setLayout(new GridBagLayout());
      jPServidor
          .setBorder(javax.swing.BorderFactory
              .createTitledBorder(
                  null,
                  "Servidor",
                  javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                  javax.swing.border.TitledBorder.DEFAULT_POSITION,
                  null, null));
      jPServidor.add(jLPuertoServidorS, gridBagConstraints3);
      jPServidor.add(getJTFPuertoServidorS(),
          gridBagConstraints4);
      jPServidor
          .add(getJBEsperarConexion(), gridBagConstraints5);
    }
    return jPServidor;
  }

  /**
   * This method initializes jTFPuertoServidorS
   * 
   * @return javax.swing.JTextField
   */
  private JTextField getJTFPuertoServidorS() {
    if (jTFPuertoServidorS == null) {
      jTFPuertoServidorS = new JTextField();
      jTFPuertoServidorS.setText("2222");
    }
    return jTFPuertoServidorS;
  }

  /**
   * This method initializes jBEsperarConexion
   * 
   * @return javax.swing.JButton
   */
  private JButton getJBEsperarConexion() {
    if (jBEsperarConexion == null) {
      jBEsperarConexion = new JButton();
      jBEsperarConexion.setText("Esperar Conexion");
      jBEsperarConexion
          .addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(
                java.awt.event.ActionEvent e) {
              esperarConexionAction();
            }
          });
    }
    return jBEsperarConexion;
  }

  protected void esperarConexionAction() {
    try {
      int puerto = Integer
          .parseInt(jTFPuertoServidorS.getText());
      String nombre = jTFNombre.getText();
      esperarConexion(puerto, nombre);
    } catch (Exception ex) {
      error(ex);
    }
  }

  protected void error(final Exception ex) {

    if (SwingUtilities.isEventDispatchThread()) {
      errorEDT(ex);
    } else {
      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          errorEDT(ex);
        }
      });
    }
  }

  private void errorEDT(final Exception ex) {
    JOptionPane.showMessageDialog(this, "Error: "
        + ex.getMessage());
    ex.printStackTrace();
    //System.exit(-1);
  }

  protected abstract void esperarConexion(int puerto,
      String nombre) throws Exception;

  /**
   * This method initializes jPanelNombre
   * 
   * @return javax.swing.JPanel
   */
  private JPanel getJPanelNombre() {
    if (jPanelNombre == null) {
      BorderLayout borderLayout1 = new BorderLayout();
      borderLayout1.setHgap(10);
      borderLayout1.setVgap(10);
      jLNombre = new JLabel();
      jLNombre.setText("Nombre");
      jPanelNombre = new JPanel();
      jPanelNombre.setLayout(borderLayout1);
      jPanelNombre
          .setBorder(javax.swing.BorderFactory
              .createTitledBorder(
                  null,
                  "Datos Personales",
                  javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                  javax.swing.border.TitledBorder.DEFAULT_POSITION,
                  null, null));
      jPanelNombre.add(jLNombre, java.awt.BorderLayout.WEST);
      jPanelNombre.add(getJTFNombre(),
          java.awt.BorderLayout.CENTER);
    }
    return jPanelNombre;
  }

  /**
   * This method initializes jTFNombre
   * 
   * @return javax.swing.JTextField
   */
  private JTextField getJTFNombre() {
    if (jTFNombre == null) {
      jTFNombre = new JTextField();
    }
    return jTFNombre;
  }

  protected void esperandoConexion() {

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        getDialogoEspera().setVisible(true);
      }
    });

  }

  private JDialog getDialogoEspera() {

    if (dialogo == null) {
      dialogo = new JDialog(this, true);
      dialogo.setTitle("Esperando conexi�n...");
      JPanel p = new JPanel(new GridBagLayout());
      p.setBorder(BorderFactory
          .createEmptyBorder(10, 10, 10, 10));
      dialogo.setContentPane(p);

      GridBagConstraints c = new GridBagConstraints();
      c.gridx = 0;
      c.gridy = 0;
      c.anchor = GridBagConstraints.CENTER;
      c.weighty = 1;
      p.add(new JLabel("Esperando conexi�n..."), c);

      c.gridy = 1;
      c.weighty = 0;
      JButton botonCancel = new JButton("Cancelar");
      botonCancel.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          dialogo.dispose();
          esperaCancelada();
        }
      });
      p.add(botonCancel, c);
      dialogo.setSize(300, 150);
      dialogo.setLocationRelativeTo(this);
    }
    return dialogo;
  }

  protected abstract void esperaCancelada();

} // @jve:decl-index=0:visual-constraint="10,10"
