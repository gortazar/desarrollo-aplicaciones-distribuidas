package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.sidelab.Ejemplorest3Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Ejemplorest3Application.class)
@WebAppConfiguration
public class Ejemplorest3ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
