package tablonanuncios;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TablonController {

	private List<Anuncio> anuncios = new ArrayList<>();

	@RequestMapping("/")
	public String tablon(Model model) {

		model.addAttribute("anuncios", anuncios);

		return "tablon";
	}

	@RequestMapping("/insertar")
	public String insertar(Anuncio anuncio, Model model) {

		anuncio.setId(anuncios.size());
		anuncios.add(anuncio);

		return "insertar";
	}

	@RequestMapping("/mostrar")
	public String mostrar(@RequestParam int id, Model model) {

		Anuncio anuncio = anuncios.get(id);

		model.addAttribute("anuncio", anuncio);

		return "mostrar";
	}
}