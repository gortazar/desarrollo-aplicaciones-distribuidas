package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.sidelab.sd.airline.RestAirlinesApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RestAirlinesApplication.class)
public class RestAirlinesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
