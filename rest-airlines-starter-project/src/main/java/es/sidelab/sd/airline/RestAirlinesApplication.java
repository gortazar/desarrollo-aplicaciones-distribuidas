package es.sidelab.sd.airline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAirlinesApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestAirlinesApplication.class, args);
    }
}
