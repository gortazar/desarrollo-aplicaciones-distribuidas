package es.sidelab.sd.airline;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.sidelab.sd.model.Airline;
import es.sidelab.sd.model.Route;

@Component
public class AirlinesService {
	
	List<Airline> airlines = new ArrayList<Airline>();
	
	public AirlinesService() {
		Route r1 = new Route("r1", "Madrid", "Paris", "10am", 120);
		Route r2 = new Route("r2", "Barcelona", "Milan", "10pm", 90);
		Airline easyPlane = new Airline("easyplane", r1, r2);
		airlines.add(easyPlane);
		
		Route r3 = new Route("r3", "Dublin", "Munich", "10am", 120);
		Route r4 = new Route("r4", "NYC", "Paris", "10pm", 90);
		Airline lastChance = new Airline("lastchance", r3, r4);
		airlines.add(lastChance);

	}

	List<String> getAirlines() {
		List<String> airlineNames = new ArrayList<String>();
		for(Airline a : airlines) {
			airlineNames.add(a.getUniqueName());
		}
		return airlineNames;
	}
	
	Airline getAirline(String airlineId) {
		for(Airline a : airlines) {
			if(a.getUniqueName().equals(airlineId)) {
				return a;
			}
		}
		return null;
	}
	
	List<Route> getRoutes(String destination) {
		List<Route> routes = new ArrayList<Route>();
		for(Airline a : airlines) {
			for(Route r : a.getRoutes()) {
				if(r.getDestination().equals(destination)) {
					routes.add(r);
				}
			}
		}
		return routes;
	}
}
