package es.sidelab.sd.model;

public class Route {

	public Route(String id, String origin, String destination, String departure, int duration) {
		this.id = id;
		this.origin = origin;
		this.destination = destination;
		this.departureTime = departure;
		this.duration = duration;
	}
	
	String id;
	String origin;
	String destination;
	String departureTime;
	int duration;
	
	public String getId() {
		return id;
	}
	
	public String getOrigin() {
		return origin;
	}
	public String getDestination() {
		return destination;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public int getDuration() {
		return duration;
	}
	
	
	
}
