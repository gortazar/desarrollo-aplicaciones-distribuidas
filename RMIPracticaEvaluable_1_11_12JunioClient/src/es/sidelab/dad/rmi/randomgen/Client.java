package es.sidelab.dad.rmi.randomgen;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class Client {

	public static void main(String[] args) {
		
		try {
			
			Registry registry = LocateRegistry.getRegistry();
			RandomNumberGeneratorService service = (RandomNumberGeneratorService) registry.lookup("rangen");
			
			List<Integer> seqPatxi1 = service.generateIntegerRandomSequence(10, "patxi");
			List<Integer> seqJuanjo1 = service.generateIntegerRandomSequence(5, "juanjo");
			List<Double> seqPedro1 = service.generateDoubleRandomSequence(20, "pedro");

			List<Integer> seqPatxi2 = service.generateIntegerRandomSequence(5, "patxi");
			
			System.out.println(seqPatxi1);
			System.out.println(seqPatxi2);
			System.out.println(seqJuanjo1);
			System.out.println(seqPedro1);

		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		
	}
}
