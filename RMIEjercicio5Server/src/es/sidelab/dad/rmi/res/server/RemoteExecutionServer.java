package es.sidelab.dad.rmi.res.server;

import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import es.sidelab.dad.rmi.res.RemoteComputingService;

public class RemoteExecutionServer {

	public static void main(String[] args) {
		
		System.setSecurityManager(new RMISecurityManager());
		
		RemoteComputingServiceImpl resImpl = new RemoteComputingServiceImpl();
		
		try {
			
			RemoteComputingService res = (RemoteComputingService) UnicastRemoteObject.exportObject(resImpl, 0, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
			
			Registry registry = LocateRegistry.getRegistry();
			registry.bind("rtask", res);
			
			System.out.println("Waiting for tasks...");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
		
	}
	
}
