package teamsservice.client;

import java.util.List;

import teamsservice.client.ws.Player;
import teamsservice.client.ws.Team;
import teamsservice.client.ws.Teams;
import teamsservice.client.ws.TeamsService;


public class TeamsClient {

	public static void main(String[] args) {
		
		TeamsService service = new TeamsService();
        Teams port = service.getTeamsPort();
        List<Team> teams = port.getTeams();
        for (Team team : teams) {
            System.out.println("Team name: " + team.getName());
            for (Player player : team.getPlayers()){
                System.out.println("  Player: " + player.getNickname());
            }
        }
		
	}
	
}
