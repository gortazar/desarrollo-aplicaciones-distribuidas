package teamsservice.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;


public class TeamsClientJSON2 {

	public static void main(String[] args) throws Exception {

		// All Teams
		URL url = new URL("http://localhost:8888/teams");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Accept", "application/json");
		conn.connect();

		ObjectMapper mapper = new ObjectMapper(); 
	    mapper.getDeserializationConfig().setAnnotationIntrospector(new JaxbAnnotationIntrospector());
		TeamList teams = mapper.readValue(conn.getInputStream(), TeamList.class);

		for (Team team : teams.getList()) {
            System.out.println("Team name: " + team.getName() +
                               " (roster count: " + team.getRosterCount() + ")");
            for (Player player : team.getPlayers())
                System.out.println("  Player: " + player.getNickname());
        }		

	}

}
