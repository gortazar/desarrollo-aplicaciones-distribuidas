package es.urjc.code.dad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Ejercicioweb3Application {

	@Bean
	public UserService userService() {
		return new FakeUserService();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Ejercicioweb3Application.class, args);
	}
}
