package es.urjc.code.dad;

public class DBUserService implements UserService {

	@Override
	public int getNumUsers() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} // Sleep 0.5 sec to simulate db access
		
		return 10;
	}

}
